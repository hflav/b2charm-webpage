import os
import pathlib
from shutil import copy,rmtree
from b2charm.models import Parameters
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from datetime import date
from copy import deepcopy
import math

experiment_colors = {
    'ATLAS': 'black',
    'BaBar': 'green',
    'Belle': 'blue',
    'CDF': 'red',
    'CMS': 'orange',
    'D0': 'magenta',
    'LHCb': 'cyan',
    }

dpi = 120


def hflav_logo(fig, scale=1):
    """Add the HFLAV logo to the figure"""

    subtitle = str(date.today())
    ypixel = 25
    xyratio = 4.8
    ysub = 0.9
    fontsize = 12
    fontsub = 0.75
    offset = -0.05
    xsize, ysize = fig.transFigure.inverted().transform((scale * xyratio * ypixel, scale * (1 + ysub) * ypixel))
    fraction = ysub / (1 + ysub)
    font = {'family': 'sans-serif', 'style': 'italic', 'color': 'white', 'weight': 'bold', 'size': scale * fontsize}

    save_axes = plt.gca()
    ax = plt.axes((0, 1-ysize, xsize, ysize), label='logo', frame_on=False)
    ax.set_axis_off()
    plt.fill([0, 1, 1, 0], [fraction, fraction, 1, 1], 'k', edgecolor='k', linewidth=0.5)
    plt.text(0.5, 0.5 * (1 + fraction) + offset, 'HFLAV', fontdict=font, ha='center', va='center')
    plt.fill([0, 1, 1, 0], [0, 0, fraction, fraction], 'w', edgecolor='k', linewidth=0.5)
    font['color'] = 'black'
    font['size'] *= fontsub
    plt.text(0.5, 0.5 * fraction + offset, subtitle, fontdict=font, ha='center', va='center')
    plt.sca(save_axes)

def plot_measurement(y, value, stat_error, syst_error, xmin, color = 'black', linestyle = 'solid'):
    """Add a measurement entry to a plot."""

    if value is None:
        return
    if stat_error is None:
        offset = 0 if xmin is None else xmin
        plot = plt.errorbar([value], [y], xerr=[[value - offset], [0]], xuplims=True, capsize=6, mew=2, color=color, linestyle=linestyle)
        plot[1][0].set_marker(matplotlib.markers.CARETLEFT)
        plot[1][0].set_markeredgewidth(0)
    else:
        error = deepcopy(stat_error)
        if syst_error is not None:
            error = add(error,syst_error)
        plot = plt.errorbar([value], [y], xerr=[[abs(error[1])], [error[0]]], fmt='o', capsize=0, color=color, linestyle=linestyle)
        plt.errorbar([value], [y], xerr=[[abs(stat_error[1])], [stat_error[0]]], capsize=6, mew=2, color=color)

    return plot

def add(error1, error2):
        """Quadratically add the given error."""
        error = []
        error.append(math.sqrt(error1[0]*error1[0] + error2[0]*error2[0]))
        error.append(-1*math.sqrt(error1[1]*error1[1] + error2[1]*error2[1]))
        return error

def min_max_limit(entries, log=False):
    """Get range covered by values (entry[0]) with statistical (entry[1]) and systematic (entry[2]) errors and determine if they contain a limit. If log is True negative values are ignored."""

    min = None
    max = None
    limit = False
    for entry in entries:
        if entry[0] is None:
            continue
        lower = upper = entry[0]
        if entry[1] is None:
            limit = True
        else:
            error = deepcopy(entry[1])
            if entry[2] is not None:
                error = add(error,entry[2])
            lower += error[1]
            upper += error[0]
        if (min is None or min > lower) and (not log or lower > 0):
            min = lower
        if max is None or max < upper:
            max = upper

    return (min, max, limit)

def result_plot(parameter_data, filename, options=None):
    """Create a plot of average and individual measurements."""

    # select measurements with information about the parameter of interest and check if they contain a limit
    measurements = parameter_data["measurements"]
    entries = []
    for measurement in measurements:
        if 'value' in measurement.keys() and measurement['value'] is not None:
            if 'stat_error' in measurement.keys() or 'stat_error_pos' in measurement.keys():
                stat_error = []
                if 'stat_error_pos' in measurement.keys():
                    stat_error.append(measurement['stat_error_pos'])
                    stat_error.append(measurement['stat_error_neg'])
                else:
                    stat_error.append(measurement['stat_error'])
                    stat_error.append(-1*measurement['stat_error'])
            else:
                stat_error = None
            if 'syst_error' in measurement.keys() or 'syst_error_pos' in measurement.keys():
                syst_error = []
                if 'syst_error_pos' in measurement.keys():
                    syst_error.append(measurement['syst_error_pos'])
                    syst_error.append(measurement['syst_error_neg'])
                else:
                    syst_error.append(measurement['syst_error'])
                    syst_error.append(-1*measurement['syst_error'])
            else:
                syst_error = None
            entries.append([measurement['value'], stat_error, syst_error])
    xmin, xmax, limit = min_max_limit(entries)
    xmin = 0 if (limit and xmin > 0) else None
    # create the plot axes
    n = len(measurements)
    dy = 1. / (n + 1)
    fig = plt.figure(figsize=(8, (n+3) * 0.4), dpi=dpi, constrained_layout=True)
    plt.title(f'${parameter_data["latex"]}$')
    plt.axis(ymin=0, ymax=1)
    plt.yticks([])

    # plot the average
    if "error" not in parameter_data.keys() and "error_pos" not in parameter_data.keys():
        plt.bar(0, 1, parameter_data["value"], 0, align='edge', color='yellow')
    else:
        if "error_pos" in parameter_data.keys():
            plt.axvspan(parameter_data["value"] + parameter_data["error_neg"], parameter_data["value"] + parameter_data["error_pos"], color='yellow')
            plt.axvline(parameter_data["value"], color='black')
        else:
            plt.axvspan(parameter_data["value"] - parameter_data["error"], parameter_data["value"] + parameter_data["error"], color='yellow')
            plt.axvline(parameter_data["value"], color='black')

    # plot the PDG average
    if "pdg_value" in parameter_data.keys():
        if "pdg_error" not in parameter_data.keys() and "pdg_error_pos" not in parameter_data.keys():
            plt.bar(0, 1, parameter_data["pdg_value"], 0, align='edge', color='lightsteelblue', alpha=0.5)
        else:
            if "pdg_error_pos" in parameter_data.keys():
                plt.axvspan(parameter_data["pdg_value"] + parameter_data["pdg_error_neg"], parameter_data["pdg_value"] + parameter_data["pdg_error_pos"], color='lightsteelblue', alpha=0.5)
                plt.axvline(parameter_data["pdg_value"], color='black', alpha=0.5)
            else:
                plt.axvspan(parameter_data["pdg_value"] - parameter_data["pdg_error"], parameter_data["pdg_value"] + parameter_data["pdg_error"], color='lightsteelblue', alpha=0.5)
                plt.axvline(parameter_data["pdg_value"], color='black', alpha=0.5)

    # plot each measurement
    y = n * dy
    legend_entries = []
    legend_labels = []
    for measurement in measurements:
        if 'value' in measurement.keys() and measurement['value'] is not None:
            color = experiment_colors.get(measurement["experiment"], 'black')
            if 'stat_error' in measurement.keys() or 'stat_error_pos' in measurement.keys():
                stat_error = []
                if 'stat_error_pos' in measurement.keys():
                    stat_error.append(measurement['stat_error_pos'])
                    stat_error.append(measurement['stat_error_neg'])
                else:
                    stat_error.append(measurement['stat_error'])
                    stat_error.append(-1*measurement['stat_error'])
            else:
                stat_error = None
            if 'syst_error' in measurement.keys() or 'syst_error_pos' in measurement.keys():
                syst_error = []
                if 'syst_error_pos' in measurement.keys():
                    syst_error.append(measurement['syst_error_pos'])
                    syst_error.append(measurement['syst_error_neg'])
                else:
                    syst_error.append(measurement['syst_error'])
                    syst_error.append(-1*measurement['syst_error'])
            else:
                syst_error = None
            plot = plot_measurement(y, measurement['value'], stat_error,syst_error, xmin, color)
            if measurement["experiment"] not in legend_labels:
                legend_labels.insert(0, measurement["experiment"])
                legend_entries.insert(0, plot)
            y -= dy

    if xmin is not None:
        plt.xlim(left=xmin)

    # draw legend, logo and save figure
    if "pdg_value" in parameter_data :
        legend_labels.insert(0, 'PDG')
        legend_entries.insert(0, (matplotlib.patches.Patch(color='lightsteelblue', alpha=0.5), matplotlib.lines.Line2D([], [], color='black', marker='|', linestyle='None', alpha=0.5)))
    legend_labels.insert(0, 'HFLAV')
    legend_entries.insert(0, (matplotlib.patches.Patch(color='yellow'), matplotlib.lines.Line2D([], [], color='black', marker='|', linestyle='None')))
    try:
        plt.legend(legend_entries, legend_labels)
    except:
        print('WARNING: Plotting of legend failed.')
    hflav_logo(fig)
    try:
        plt.savefig(filename, format='png', dpi=dpi)
    except Exception as exception:
        print('WARNING: The saving of %s failed:' % filename)
        print(exception)
    plt.close()

def run():
    ''' this function creates averaging plot for each branching fraction and stores them in the static folder'''
    present_path = pathlib.Path().resolve()
    dest_path = str(present_path) + "/b2charm/static/b2charm/images/"
    try:
        rmtree(dest_path)
        os.mkdir(dest_path)
    except:
        os.mkdir(dest_path)
    all_obj = Parameters.objects.all()
    for obj in all_obj:
        print ("plotting done for:  ",str(obj.data["id"]))
        filename = dest_path+ str(obj.data["id"])+'.png'
        result_plot(obj.data,filename)

    
    
    
