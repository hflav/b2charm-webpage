import os
import json
from b2charm.models import Parameters
import pathlib


def run():
    ''' This function iterates through all JSON files in the "dump" directory and creates a new entry in database , table ->
    Parameters if that branching fraction wasn't present earlier in the table or updates the older entry related to that 
    branching fraction.'''
    present_path = pathlib.Path().resolve()
    dump_path = str(present_path) + "/dump"
    for filename in os.listdir(dump_path):
        if filename.endswith(".json") :
            file_path = os.path.join(dump_path, filename)
            f = open(file_path)
            data = json.load(f)
            id = data["id"]
            try:
                par = Parameters.objects.filter(data__id=id).first()
                par.data = data
                par.save()
                print (filename, " : Parameter already exists in database, Values updated")
            except:
                par = Parameters()
                par.data = data
                par.save()
                print (filename, " : New Parameter added to database")
            f.close()
            
            
