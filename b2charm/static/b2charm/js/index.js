$(document).ready(function () {
    function formsubmit() {
        $("#filter_form").submit(function (e) {
            return false; //prevent default form submit
        });
        var serializedData = $('#filter_form').serialize(); 
        $.ajax({
            url: "/index/post/ajax/filter",
            type: "POST",
            data: serializedData,
            success: function (json_response) {
                // render a datatable upon succesful ajax request, used data retured by ajax call to show in the table.
                globalThis.table = $('#table1').DataTable({
                    data: JSON.parse(json_response),
                    responsive: true,
                    bDestroy: true,
                    searching: false,
                    "ordering": false,
                    "pageLength": 25,
                    "sDom": '<"top"flp>rt<"bottom"i><"clear">',
                    drawCallback: function () {
                        $('.paginate_button', this.api().table().container())
                            .on('click', function () {
                                MathJax.typeset();
                            });
                        $('select[name=table1_length]', this.api().table().container()).on('change', function (e) {
                            MathJax.typeset();
                        });
                    },
                    columns: [
                        { data: 'id' },
                        { data: 'latex' },
                        { data: 'value' },
                        {
                            "data": "id", "name": "id",
                            fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
                                // add redirect url to each branching fraction's individual details page
                                if (oData.id) {
                                    $(nTd).html("<a target='_blank' href='detail/" + oData.id + "'>" + "Detail" + "</a>");
                                }
                            }
                        }

                    ],
                    'columnDefs': [
                        {
                            'targets': 0, // select column 0 for 'Select for Overview Plot' checkboxes , part of jquery-datatables-checkboxes plugin used 
                            'checkboxes': {
                                'selectRow': true,

                            },

                        }

                    ],
                    'select': {
                        'style': 'multi'
                    },
                    'order': [[2, 'asc']]

                });
                MathJax.typeset(); // reset mathjax after ajax data loaded in table, so that mathjax can be applied on the fresh data
                $('th.sorting_disabled.dt-checkboxes-cell.dt-checkboxes-select-all').html("Select for Overview Plot");
            },
            error: function (xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
            }
        });
        
        



    }
    formsubmit();
    $('.formbtn').on('click', function (event) {
        formsubmit();

    });

    $('#clear').on('click', function (e) {
        // invoked when clear all filters button is clicked
        $('input[name=initial]').prop('checked', false);
        $('input[name=observable]').prop('checked', false);
        $('.box_particle button').each(function (index, element) {
            var btn_id = $(element).attr('id');
            var input_id = "#id_" + btn_id.slice(0, -3);
            var btn_selector = "#" + btn_id.slice(0, -3);
            var counter = $(input_id).val();
            if (counter != "0") {
                $(btn_selector).css({ "display": "none" });
                $(element).css({ "background-color": "#ddd", "border": "2px solid #444" });
                $(element).hover(function () {
                    $(element).css({ "background-color": "#dfd" });
                }, function () {
                    $(element).css({ "background-color": "#ddd" });
                });
                $(input_id).val("0");
            }

        });
        formsubmit();

    });
    $('.minus').click(function () {
        // invoked when '-' is clicked in a child numbers entry
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        formsubmit();

    });
    $('.plus').click(function () {
        // invoked when '+' is clicked in a child numbers entry
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        formsubmit();

    });
    $('.box_particle button').click(function (e) {
        e.preventDefault();
        // invoked when a filter button is clicked
        var btn_id = $(this).attr('id');
        var input_id = "#id_" + btn_id.slice(0, -3);
        var btn_selector = "#" + btn_id.slice(0, -3);
        var counter = $(input_id).val();
        if (counter == "0") {
            $(btn_selector).css({ "display": "block" });
            $(this).css({ "background-color": "#bfb", "border-color": "#4c4" });
            $(this).hover(function () {
                $(this).css({ "background-color": "#bfb" });
            }, function () {
                $(this).css({ "background-color": "#bfb" });
            });
            $(input_id).val("1");
            formsubmit();
        }
        else {
            $(btn_selector).css({ "display": "none" });
            $(this).css({ "background-color": "#ddd", "border": "2px solid #444" });
            $(this).hover(function () {
                $(this).css({ "background-color": "#dfd" });
            }, function () {
                $(this).css({ "background-color": "#ddd" });
            });
            $(input_id).val("0");
            formsubmit();
        }
        return false;
    });

    $("input:radio").on("click", function (e) {
        var inp = $(this);
        if (inp.is(".theone")) {
            inp.prop("checked", false).removeClass("theone");
            formsubmit();
        } else {
            $("input:radio[name='" + inp.prop("name") + "'].theone").removeClass("theone");
            inp.addClass("theone");
            formsubmit();
        }

    });

    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $("#toTop").click(function () {
        $("html, body").animate({ scrollTop: 0 }, 500);
    });




});

$(document).on({
    ajaxStart: function () {
        $("body").addClass("loading");
    },
    ajaxStop: function () {
        $("body").removeClass("loading");
    }
});