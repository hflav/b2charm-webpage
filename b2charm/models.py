from django.db import models
from django.contrib.postgres.fields import JSONField
from django.db.models.fields import CharField
from datetime import datetime,timezone
import os
 
class Parameters(models.Model):
    ''' Parameters table with only an id and a data field, "data" field stores all content about a branching fraction
    in JSON format. '''
    data = JSONField()

class plot_info(models.Model):
    ''' Stores image id and image path for a user generated overview plot '''
    img_id = models.UUIDField(unique=True)
    img_path = models.CharField(max_length=2000)
    created_on = models.DateTimeField(auto_now_add=True,null=True)

    def chk_expiry(self):
        ''' This function checks whether a user generated overview plot has expired i.e if that plot has crossed the deletion time
        , if it is past the deletion time then that plot is deleted from the database as well as from the directory storing these plots'''
        deletion_time = 5 #in minutes
        now = datetime.now(timezone.utc)
        img_objs = plot_info.objects.all()
        for img_obj in img_objs:
            created=img_obj.created_on
            td=(now-created).total_seconds()
            if deletion_time==0:
                pass
            elif td >= (deletion_time*60):
                try:
                    os.remove(img_obj.img_path)
                except:
                    continue
                try:
                    img_obj.delete()
                except:
                    continue
                
class PageView(models.Model):
    ''' Stores PageViews count for the website '''
    hostname = models.CharField(max_length=32)
    timestamp = models.DateTimeField(auto_now_add=True)
