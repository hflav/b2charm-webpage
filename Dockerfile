FROM registry.access.redhat.com/ubi8/python-38

# Add application sources to a directory that the assemble script expects them
# and set permissions so that the container runs without root access
USER 0
ADD . /tmp/src
ADD run /usr
RUN /usr/bin/fix-permissions /tmp/src
USER 1001

# Install the dependencies
RUN /tmp/src/assemble

# Set the default command for the resulting image
CMD /usr/run